package ru.vaa.pet;

public class Dog extends Pet {

    public Dog(String name, int LevelOfSatiety) {
        super(name, LevelOfSatiety);
    }

    void game() {
        LevelOfSatiety -= (int) (Math.random() * 31);
        if (LevelOfSatiety < 0) {
            LevelOfSatiety = 0;
        }
    }

    void food() {
        LevelOfSatiety += (int) (Math.random() * 50);
        if (LevelOfSatiety > 100) {
            LevelOfSatiety = 100;
        }
    }

    public int getLevelOfSatiety() {
        return LevelOfSatiety;
    }

    @Override
    public String toString() {
        return "Пес{" +
                "\nкличка: " + name +
                "\n}";
    }
}