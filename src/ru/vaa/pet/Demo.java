package ru.vaa.pet;

public class Demo {

    public static void main(String[] args) {
        Pet cat = new Cat("Васька", (int) (Math.random() * 100));
        Pet dog = new Dog("Тайсон", (int) (Math.random() * 100));
        Pet hamster = new Hamster("Хома", (int) (Math.random() * 100));
        for (int clock = 12; clock < 21; clock++) {
            cat.game();
            cat.food();

            dog.game();
            dog.food();

            hamster.game();
            hamster.food();
        }
        Pet[] pets = {cat, dog, hamster};
        hangrypet(pets);
    }

    private static void hangrypet(Pet[] pets) {
        if (pets[0].getLevelOfSatiety() < pets[1].getLevelOfSatiety() && pets[0].getLevelOfSatiety() < pets[2].getLevelOfSatiety()) {
            System.out.println("Самое голодное животное к 20:00 оказался: " + pets[0].toString());
        }
        if (pets[1].getLevelOfSatiety() < pets[0].getLevelOfSatiety() && pets[1].getLevelOfSatiety() < pets[2].getLevelOfSatiety()) {
            System.out.println("Самое голодное животное к 20:00 оказался: " + pets[1].toString());
        }
        if (pets[2].getLevelOfSatiety() < pets[1].getLevelOfSatiety() && pets[2].getLevelOfSatiety() < pets[0].getLevelOfSatiety()) {
            System.out.println("Самое голодное животное к 20:00 оказался : " + pets[2].toString());
        }
    }
}
