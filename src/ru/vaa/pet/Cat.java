package ru.vaa.pet;

public class Cat extends Pet {

    public Cat(String name, int LevelOfSatiety) {
        super(name, LevelOfSatiety);
    }

    void game() {
        LevelOfSatiety -= (int) (Math.random() * 15);
        if (LevelOfSatiety < 0) {
            LevelOfSatiety = 0;
        }
    }

    void food() {
        LevelOfSatiety += (int) (Math.random() * 27);
        if (LevelOfSatiety > 100) {
            LevelOfSatiety = 100;
        }
    }

    public int getLevelOfSatiety() {
        return LevelOfSatiety;
    }

    @Override
    public String toString() {
        return "Кот{" +
                "\nКличка: " + name +
                "\n}";
    }
}
