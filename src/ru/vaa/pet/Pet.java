package ru.vaa.pet;

public abstract class Pet {
    String name;
    int LevelOfSatiety;

    public Pet(String name, int LevelOfSatiety) {
        this.name = name;
        this.LevelOfSatiety = LevelOfSatiety;
    }

    abstract void game();

    abstract void food();

    public abstract int getLevelOfSatiety();
}
