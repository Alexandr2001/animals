package ru.vaa.pet;

public class Hamster extends Pet {

    public Hamster(String name, int LevelOfSatiety) {
        super(name, LevelOfSatiety);
    }

    void game() {
        LevelOfSatiety -= (int) (Math.random() * 40);
        if (LevelOfSatiety < 0) {
            LevelOfSatiety = 0;
        }
    }

    void food() {
        LevelOfSatiety += (int) (Math.random() * 40);
        if (LevelOfSatiety > 100) {
            LevelOfSatiety = 100;
        }
    }

    public int getLevelOfSatiety() {
        return LevelOfSatiety;
    }

    @Override
    public String toString() {
        return "Хомяк{" +
                "\nКличка: " + name +
                "\n}";
    }
}